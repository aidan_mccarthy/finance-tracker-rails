# ArticlesController
class StocksController < ApplicationController
  def search
    if params[:stock].blank?
      flash.now[:danger] = 'Please enter a symbol to search'
    else
      @stock = Stock.new_from_lookup(params[:stock])
      flash.now[:danger] = 'Could not find symbol' unless @stock
    end
    respond_to do |format|
      format.js { render partial: 'users/result' }
    end
  end
end
